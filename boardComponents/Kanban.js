import React, { useEffect, useState } from 'react';
import TaskCard from '../pages/board/TaskCard';
import dynamic from 'next/dynamic';
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";

const Kanban = (props) => {
  const {columns, setColumns} = props;
  const [isLoading, setIsLoading] = useState(true);
  //const [data, setData] = useState([])

  const DragDropContext = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.DragDropContext;
      }),
    {ssr: false},
  );
  
  const Droppable = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.Droppable;
      }),
    {ssr: false},
  );

  const onDragEnd = (result, columns, setColumns) => {
    if (!result.destination) return;
    const { source, destination } = result;
    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  useEffect(() => {
    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/')
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setData(data.data)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

}, [])

  return (
    <DragDropContext
      onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
    >
        <div className='grid grid-cols-12 mt-[20px] gap-3'>
        {Object.entries(columns).map(([columnId, column], index) => {
            return (
            <Droppable key={index} droppableId={columnId}>
                {(provided) => (
                <div
                    className='col-span-12 md:col-span-12 lg:col-span-4  bg-[#f3f3f3] rounded-[20px] pl-[15px] pt-[15px]'
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                >
                    <div className='bg-[#582BE7] w-[100px] mt-[15px] pl-[8px] mb-[15px] rounded-[20px] text-[#fff] font-semibold'>
                    
                      {column.title !== null && column.title !== undefined ?
                        <div>
                            {column.title}

                        </div>
                      : <></>
                      }
                    </div>
                    
                    {isLoading ?
                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                        <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                    </div>
                    :
                    
                    <div>
                    {column.items.map((item, index) => (
                    <TaskCard key={index} item={item} index={index} />
                    ))}
                    </div>
                    }

                    {provided.placeholder}
                </div>
                )}
            </Droppable>
            );
        })}
        </div>
      
    </DragDropContext>
  );
};

export default Kanban;