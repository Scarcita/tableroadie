import '../styles/globals.css'
import Router from 'next/router';
import  NProgress  from 'nprogress';
import 'nprogress/nprogress.css'
import { SessionProvider } from 'next-auth/react';
import { AuthProvider } from './authContext' 
import Home from '.';
import Auth from '.';


Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

function MyApp({ Component,  pageProps: {session, ...pageProps} }) {

  const getLayout = Component.getLayout || ((page) => page )
  
  return (
      <SessionProvider session={session}>
        {/* <Auth> */}
        
          {getLayout(<Component {...pageProps} />)}

        {/* </Auth> */}
        
      </SessionProvider>
  );
}
export default MyApp
