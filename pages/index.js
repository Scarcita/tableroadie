import React, { useEffect, useState } from 'react';
import { signIn, signOut, useSession} from 'next-auth/react'
import 'react-activity/dist/Spinner.css'
import { useRouter } from 'next/router'

export default function Auth() {

  // if(!session) {
  //   return <>
  //     Not signed in <br/>
  //     <button onClick={() => signIn()}>Sign in</button>
  //   </>
  // }
  //const session = await getSession({ req })
  const [isLoading, setisLoading] = useState(true)
  const router = useRouter();
  const { data: session, status } = useSession()
  const hasUser = !session?.user;
  const role = hasUser.role;

  let allowed = true;
  
  if (router.pathname.startsWith("/plans") && role !== "ADMIN") {
    allowed = false;
  }
  if (router.pathname.startsWith("/dashboard") && role !== "ADMIN") {
    allowed = false;
  }
  if (router.pathname.startsWith("/calendar") && role !== "ADMIN") {
    allowed = false;
  }
  if (router.pathname.startsWith("/clients") && role !== "ADMIN") {
    allowed = false;
  }
  if (router.pathname.startsWith("/board") && role !== "ADMIN") {
    allowed = false;
  }
  if (router.pathname.startsWith("/ads") && role !== "ADMIN") {
  allowed = false;
  }
  if (router.pathname.startsWith("/board") && role !== "designer") {
      allowed = false;
  }
  if (router.pathname.startsWith("/calendar") && role !== "manager") {
      allowed = false;
  }
  if (router.pathname.startsWith("/clients") && role !== "manager") {
  allowed = false;
  }
  if (router.pathname.startsWith("/calendar") && role !== "community") {
  allowed = false;
  }
  if (router.pathname.startsWith("/dashboard") && role !== "accountant") {
  allowed = false;
  }
  if (router.pathname.startsWith("/clients") && role !== "accountant") {
  allowed = false;
  }
  if (router.pathname.startsWith("/ads") && role !== "accountant") {
  allowed = false;
  }

  // if (role.user.role === "board") {
  //   await router.replace("/ADMIN");
  // }
  // if (appState.user.role === "customer") {
  //     await router.replace("/customer");
  // }

  useEffect(() => {
    if (!isLoading && !role) {
      router.push("/login");
    }
    }, [isLoading, role]);
    
    if (!role) {

      return <>
        You are not allowed to access that location <br/>
        <button onClick={() => router.back()}>Go back</button>
      </>
    }


}

