import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';


export default function CounterDashoard() {

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([ ]);
    const router = useRouter()
    const {client_id} = router.query


    useEffect(() => {
        fetch('https://slogan.com.bo/vulcano/orders/total/abierto')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])

    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{data} {client_id}</div>
    )
}