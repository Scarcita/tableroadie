const Denied = () => {

    return (
        <div>
      You are not allowed to access that location <br/>
      <button onClick={() => router.back()}>Go back</button>
      </div>
      )
}

export default Denied;
