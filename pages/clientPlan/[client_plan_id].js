import React, { useEffect, useState } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { useRouter } from 'next/router'
import Layout from '../components/layout'

import TablePost from "./Components/tablePost"
import TableAds from './Components/TableAds'
import ModalPost from "./Components/ModalPost"
import ModalAds from "./Components/ModalAds"
import CounterClient from './Components/counterClient';
import StatusPost from './Components/statusPost';
import PricePlan from './Components/pricePlan';
import PriceAds from './Components/PriceAds';
import ModalExtras from './Components/ModalExtras';
import TableExtras from './Components/TableExtras';


export default function ClientPlan(props) {

    const [reloadPosts, setReloadPosts] = useState(false);
    const [reloadAds, setReloadAds] = useState(false);
    //const [reloadPrice, setReloadPrice] = useState(false);
    const [isLoading, setIsLoading] = useState(true)
    const router = useRouter()
    const { client_plan_id } = router.query;
    //const { client_id } = router.query;
    //const { client_id }= props

    useEffect(() => {

        if(router.isReady){

        }
        
    }, [router.isReady])
    

    return (
        <div className='ml-[30px] mt-[30px] mr-[30px]'>
            <div className='grid grid-cols-12'>
              <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                <div className='text-[24px] md:text-[32px] lg:text-[36px] text-[#000000] font-semibold'>
                    Client plan
                </div>
                <div>
                    <p className="text-[14px] md:text-[16px] lg:text-[18px] font-semibold text-right">Smart Start {client_plan_id}</p>
                    <p className="text-[10px] md:text-[16px] lg:text-[18px] font-medium text-right">Feb 2, 2022 Feb 8, 2022</p>

                </div>
                  
              </div>
              
            </div >

            <div>
            {router.isReady &&
                <PricePlan 
                //client_id={client_id}
                client_plan_id={client_plan_id}
                />
            }
            </div>

            <div className="mt-[24px] text-[18px] font-semibold">
                 Status
            </div>
            <div  className='grid grid-cols-12'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                {router.isReady &&
                    <StatusPost
                    //client_id={client_id}
                    client_plan_id={client_plan_id}
                    />
                }
                </div>

            </div>
            <div className="mt-[24px] text-[18px] font-semibold">
                Ads overview
            </div>
            <div>
                {router.isReady &&
                <PriceAds
                //client_id={client_id}
                client_plan_id={client_plan_id}
                />
                }
            </div>
            <div className='grid grid-cols-12 justify-left mt-10'>
                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                            <Tabs>
                                <TabList className='grid grid-cols-12'>
                                    <Tab
                                        className='col-span-4 md:col-span-4 lg:col-span-4 bg-[#F3F3F3] rounded-l-md flex outline-offset-0 shadow-md cursor-pointer h-8 flex justify-center items-center text-[12px] font-semibold text-[#000000] focus:bg-[#643DCE] focus:text-[#fff]'
                                    >
                                        POST
                                    </Tab>
                                    <Tab
                                        className='col-span-4 md:col-span-4 lg:col-span-4 bg-[#F3F3F3] flex outline-offset-0 shadow-md cursor-pointer  h-8 flex justify-center items-center text-[12px] font-semibold  text-[#000000] focus:bg-[#643DCE] focus:text-[#fff]'
                                    >
                                        ADS
                                    </Tab>
                                    <Tab
                                        className='col-span-4 md:col-span-4 lg:col-span-4 bg-[#F3F3F3] rounded-r-md flex outline-offset-0 shadow-md cursor-pointer h-8 flex justify-center items-center text-[12px] font-semibold text-[#000000] focus:bg-[#643DCE] focus:text-[#fff]'
                                    >
                                        EXTRAS
                                    </Tab>
                                </TabList>

                                <TabPanel>
                                    <div className='grid grid-cols-12 mt-[20px]'>
                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                                            <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>POSTS</div>
                                            <div className='flex flex-row'>

                                                {router.isReady &&
                                                <ModalPost client_plan_id={client_plan_id} reloadPosts={reloadPosts} setReloadPosts={setReloadPosts} />
                                                }
                                            </div>
                                            
                                        </div>
                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'>
                                        {router.isReady &&
                                            <TablePost client_plan_id={client_plan_id} reloadPosts={reloadPosts} />
                                        }
                                        </div>

                                    </div>
                                </TabPanel>

                                <TabPanel>
                                <div className='grid grid-cols-12 mt-[20px]'>
                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                                        <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>ADS</div>
                                        <div className='flex flex-row '>
                                        {router.isReady &&
                                            <ModalAds client_plan_id={client_plan_id}  reloadAds={reloadAds} setReloadAds={setReloadAds}  />
                                        }
                                        </div>
                                        
                                    </div>

                                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                                        {router.isReady &&
                                            <TableAds client_plan_id={client_plan_id} reloadAds={reloadAds} />
                                        }
                                    </div>
                                
                                </div >
                                </TabPanel>

                                <TabPanel>
                                    <div className='grid grid-cols-12 mt-[20px]'>
                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                                            <div className='text-[17px] md:text-[17px] lg:text-[17px] text-[#000000] font-semibold'>EXTRAS</div>
                                            <div className='flex flex-row '>
                                            {router.isReady &&
                                                <ModalExtras client_plan_id={client_plan_id}  reloadAds={reloadAds} setReloadAds={setReloadAds}  />
                                            }
                                            </div>
                                            
                                        </div>

                                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                                            {router.isReady &&
                                                <TableExtras client_plan_id={client_plan_id} reloadAds={reloadAds} />
                                            }
                                        </div>
                                    
                                    </div>
                                </TabPanel>
                            </Tabs>
                        </div>
                    </div>

            <div className='grid grid-cols-12 gap-3 mt-[35px]'>
              <div className='col-span-12 md:col-span-12 lg:col-span-5'>
                
                  
              </div>

              <div className='col-span-12 md:col-span-12 lg:col-span-7'>
                
                  
              </div>

              
            </div>
            {/* </div>
            )} */}
     </div>
    )
}
ClientPlan.getLayout = function getLayout(page){
    return (
      <Layout>{page}</Layout>
    )
  }