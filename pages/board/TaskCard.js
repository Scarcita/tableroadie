import React  from 'react';
import { useEffect, useState } from 'react'
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import Image from 'next/image';

import dynamic from 'next/dynamic';


const TaskCard = ({ item, index }) => {
  const [isLoading, setIsLoading] = useState(true);
  //const [data, setData] = useState([]);


  const Draggable = dynamic(
    () =>
      import('react-beautiful-dnd').then(mod => {
        return mod.Draggable;
      }),
    {ssr: false},
  );

  useEffect(() => {
    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/')
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setData(data.data)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

}, [])

  const imgUrl = "";

  if(item !== undefined){
    if(item !== null){
      if( item.social_network !== null && item.social_network !== undefined) {
        switch (item.social_network) {
          case 'Facebook': 
              imgUrl = '/SocialMedia/Facebook.svg'
              break;
          case 'TikTok':
              imgUrl= '/SocialMedia/TikTok.svg'
              break;
          case 'Instagram':
              imgUrl = '/SocialMedia/Instagram.svg'
          break;
          case 'YouTube':
              imgUrl = '/SocialMedia/Youtube.svg'
          break;
          case 'Mailing':
              imgUrl = '/Plans/gmail.svg'
          break;
          case 'LinkedIn':
              imgUrl = '/SocialMedia/messenger.svg'
          break;
          case 'Twitter':
              imgUrl = '/SocialMedia/Twitter.svg'
          break;
              
          default:
              break;
      }
      }
      
    }
  }

  const imgType = "";

  if(item !== undefined){
      if (item !== null){
        if(item.type !== null && item.type !== undefined){
          switch (item.type) {
            case 'Image': 
              imgType = '/Board/image.png'         
                break;
            case 'Album':
              imgType= '/Board/image.png'
                break;
            case 'Video':
              imgType = '/Board/video.png'
            
            break;
            case 'story':
              imgType = '/Board/video.png'
            
            break;
            case 'Reel':
              imgType = '/Board/video.png'
            
            break;
                
            default:
                break;
        }
        }
      }
  }

  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var eventdate = item.planned_datetime;
        var splitdate = eventdate.split('-');
        //console.log(splitdate);
        var day = splitdate[2];
        var year = splitdate[0];
      }
    }
  }
  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var d = new Date(item.planned_datetime);
        var dayName = days[d.getDay()];
      }
    }
  }

  if(item !== undefined){
    if (item !== null){
      if(item.planned_datetime !== null && item.planned_datetime !== undefined){
        var months = ['January', 'February ', 'March ', 'April', 'May', 'June', 'July','August', 'September', 'Octuber', 'November', 'December'];
        var m = new Date(item.planned_datetime);
        var monthName = months[m.getMonth()];
      }
    }
  }

  return (
    item !== undefined ? 
    <Draggable

      key={item.id}
      draggableId={item.id.toString()}
      index={index}
    >
      {(provided) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          className='grid grid-cols-12'
          >
          <div className='col-span-12 md:col-span-12 lg:col-span-12 min-h-[105px] rounded-[10px] bg-white mb-[15px] mr-[15px] pl-[10px] pt-[10px] pb-[10px] pr-[10px]'>
            {isLoading ?
              <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                  <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
              </div>
              :
              <div className='grid grid-cols-12 gap-3'>
                <div className='col-span-7 md:col-span-7 lg:col-span-7'>
                  <div className='grid grid-cols-12 gap-2 mt-[5px] mb-[5px]'>
                    <div className='col-span-5 md:col-span-5 lg:col-span-5'>
                      {/* {item.planned_datetime !== null ? */}
                      <div>
                        <p className='text-[36px] font-bold text-center'>{day}</p>
                        
                      </div>
                      {/* // : <></>
                      // } */}
                    </div>
                    <div className='col-span-7 md:col-span-7 lg:col-span-7'>
                      {/* {item.planned_datetime !== null ? */}
                      <div>
                        <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayName}</p>
                        <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthName} {year}</p>
                        <p className='text-[12px] font-semibold text-right leading-4 whitespace-normal'>7:00 AM</p>
                        
                      </div>
                      {/* : <></>
                      } */}
                    </div>
                  </div>
                  {item.title !== null && item.title !== undefined ?
                  <div>
                      <p className='text-[12px] font-semibold text-right leading-4 whitespace-normal'> {item.title}</p>
                  </div>
                  : <></>
                  }

                  {item.subtitle !== null && item.subtitle !== undefined ?

                    <div className='mt-[5px] mb-[5px]'>
                      <p className='text-[12px] text-[#582BE7] font-light leading-4 whitespace-normal'>{item.subtitle}</p>
                    </div>
                  : <></>
                  }
                  {item.instructions !== null && item.instructions !== undefined ?
                  <div className='mt-[5px] mb-[5px]'>
                    <div className='font-semibold text-[14px] text-[#582BE7]'>instructions: </div>
                    <p className='text-[12px] leading-4 whitespace-normal'>{item.instructions}</p>
                  </div>
                  : <></>
                  }

                  {item.post_copy !== null && item.post_copy !== undefined ?
                  <div className='mt-[5px] mb-[5px]'>
                    <div className='font-semibold text-[14px] text-[#582BE7]'>Post Copy: </div>
                    <p className='text-[12px] leading-4 whitespace-normal'>{item.post_copy}</p>
                    
                  </div>
                  : <></>
                  }
                  </div>

                <div className='col-span-5 md:col-span-5 lg:col-span-5'>
                  <div className='grid grid-cols-12'>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                      {item.media_url !== null && item.media_url !== undefined ?
                        <div>
                          <div className='absolute'>
                            <Image
                                className='rounded-md bg-[#f3f3f3]'
                                src={item.media_url}
                                alt='img'
                                //layout='fixed'
                                width={100}
                                height={100}>
                            </Image>
                          </div>
                              <div className='pl-[65px] pt-[70px]'>
                                <Image
                                src={imgUrl}
                                alt='img'
                                //layout='fixed'
                                width={30}
                                height={30}>
                                </Image>
                              </div>

                        </div>
                        : <></>
                      }

                    </div>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                      {/* {item.type !== null ? */}
                        <div className=''>
                          <p className='font-semibold text-[14px] text-[#582BE7]'>Type: </p>
                          <Image
                            className='text-center items-center'
                            src={imgType}
                            alt='img'
                            layout='fixed'
                            width={40}
                            height={40}
                          />
                        </div>
                      {/* : <></>
                      } */}
                    </div>
                  </div>


                </div>
              </div>
            }

          </div>  
        </div>
      )}
      
    </Draggable>
  : <></>
    

  );
};

export default TaskCard;
